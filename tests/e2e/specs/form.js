describe('Form', () => {
  describe('When visit form url', () => {
    const validName = 'thiago';
    const validLastname = 'brasil';
    beforeEach(() => {
      cy.visit('/form');
    });
    it('container h1 Form', () => {
      cy.contains('h1', 'Form');
    });

    describe('When fill name add lastname', () => {
      beforeEach(() => {
        cy.get('.form_input_name').type(validName);
        cy.get('.form_input_lastname').type(validLastname);
      });

      it('should has button', () => {
        cy.get('button').should('be.visible');
      });

      it('should has welcome text', () => {
        cy.get('button').should('be.visible');
        cy.contains('.form_h2_welcome_message', `Welcome ${validName} ${validLastname}`);
      });

      describe('When click button', () => {
        it('should go to home', () => {
          cy.url().should('eq', 'http://localhost:8080/form'); // => true
          cy.get('button').click();
          cy.url().should('eq', 'http://localhost:8080/'); // => true
        });
      });
    });
  });
});
